const opcion1 = document.getElementById('opcion1')
const opcion2 = document.getElementById('opcion2')
const opcion3 = document.getElementById('opcion3')

const content1 = document.getElementById('content1')
const content2 = document.getElementById('content2')
const content3 = document.getElementById('content3')


let choose = 1

const changeOpcion = () =>{

    choose == 1 ? (
        opcion1.classList.value = 'option option-active',
        content1.classList.value = 'content content-active'
    )
    : (
        opcion1.classList.value = 'option',
        content1.classList.value = 'content'
    )

    choose == 2 ? (
        opcion2.classList.value = 'option option-active',
        content2.classList.value = 'content content-active'
    )
    : (
        opcion2.classList.value = 'option',
        content2.classList.value = 'content'
    )

    choose == 3 ? (
        opcion3.classList.value = 'option option-active',
        content3.classList.value = 'content content-active'
    )
    : (
        opcion3.classList.value = 'option',
        content3.classList.value = 'content'
    )
}

opcion1.addEventListener('click', ()=> {
    choose = 1
    changeOpcion()
})

opcion2.addEventListener('click', ()=> {
    choose = 2
    changeOpcion()
})

opcion3.addEventListener('click', ()=> {
    choose = 3
    changeOpcion()
})